var block = false;
var salvar = true;
var carregar = true;
function novaHora() {
  function pad(s) {
    return (s < 10) ? '0' + s : s;
  }
  var date = new Date();
  return [date.getHours(), date.getMinutes()].map(pad).join(':');
}
function enviar(e){
  if(block === false){
    var idc = e.getAttribute("data-id");
    var esc = e.getAttribute("data-escolha");
    var next = e.getAttribute("data-next");
    var atual = e.getAttribute("data-atual");
    var dialogos = document.getElementById("d"+idc);
    var escbox = document.getElementById("esc"+idc);
    var escrevendo = document.getElementById("e"+idc);
    var resRandom = Math.floor(Math.random() * 5000);

    if(next != "fim"){
      dialogos.innerHTML += `<div><span class="msg dir">${e.innerHTML}</span></div>`;
      //document.querySelector(`#${personagem[idc].nome+idc} > span.mensagem`).innerHTML = document.querySelector(`#d${idc} > div:last-child > span`).innerHTML;
      //document.querySelector(`#${personagem[idc].nome+idc} > span.data`).innerHTML = novaHora();
      var segundos = personagem[idc].dialogos[atual].rs[esc].length;
      if(personagem[idc].dialogos[atual].rs[esc] && personagem[idc].dialogos[atual].rs[esc] !== ""){
        dialogos.innerHTML += `<div style="display:none"><span class="msg esq">${personagem[idc].dialogos[atual].rs[esc]}</span></div>`;
      }
      setTimeout(function(){
        dialogos.querySelector("div:last-child").style.display = "block";
        escrevendo.style.display = "none";
        dialogos.scrollBy(0, dialogos.offsetHeight);
        escbox.innerHTML = `
        <div class="digitando" id="e${idc}">${personagem[idc].nome} está digitando...</div>
        <a class="esc" data-escolha="0" data-id="${idc}" data-atual="${next}" data-next="${personagem[idc].dialogos[next].nx[0]}" onclick="enviar(this)">${personagem[idc].dialogos[next].op[0]}</a>
        <a class="esc" data-escolha="1" data-id="${idc}" data-atual="${next}" data-next="${personagem[idc].dialogos[next].nx[1]}" onclick="enviar(this)">${personagem[idc].dialogos[next].op[1]}</a>`;
        //document.querySelector(`#${personagem[idc].nome+idc} > span.mensagem`).innerHTML = document.querySelector(`#d${idc} > div:last-child > span`).innerHTML;
        block = false;
      },segundos*60+resRandom);
      setTimeout(function(){
        escrevendo.style.display = "block";
        localStorage.setItem(document.body.id, document.documentElement.innerHTML);
      }, resRandom);
      dialogos.scrollBy(0, dialogos.offsetHeight);
    }else{
      window.alert("Fim do teste, recarregue a pagina para recomeçar.");
    }
  }
  block = true;
}
function newBlob(text){
  var blob = new Blob(["\ufeff", text], {type: "text/html"});
  var url = window.URL.createObjectURL(blob);
  return url;
}
function janelaConversa(dataId){
  //if(!document.getElementById("w" + dataId.getAttribute("data-id"))){
  if(!localStorage.getItem("id"+dataId.getAttribute("data-id"))){
    var tel = dataId.getAttribute("data-id");
    var per = personagem[tel];
    localStorage.setItem("id"+tel, `
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>The Corruption</title>
        <link rel="stylesheet" type="text/css" href="https://app.sollic.com/fontawesome/css/all.css"/>
        <link rel="stylesheet" href="https:/app.sollic.com//style-1.0.9.css">
        <script src="https://app.sollic.com/personagens-1.0.4.js"></script>
        <script src="https://app.sollic.com/error-1.0.2.js"></script>
        <script src="https://app.sollic.com/jquery.js"></script>
      </head>
      <body id="id${tel}">
        <button onclick="localStorage.setItem(document.body.id, document.documentElement.outerHTML)">clica</button>
        <div id="w${tel}" class="wincon" style="visibility:visible;">
          <div class="header">
            <button class="btnbarranf" onclick="window.location.href='https://app.sollic.com/mensagens/'"><i class="fas fa-angle-left"></i> <div class="perfilcon" style="background-image: url(${per.imagem});"></div></button>
            <div class="ncon" onclick="document.querySelector('#w${tel} > div.detalhes').style.visibility='visible'">${per.nome}</div>
          </div>
          <div class="dialogos" id="d${tel}"></div>
          <div class="digitando" id="e${tel}">${personagem[tel].nome} está digitando...</div>
          <div class="escbox" id="esc${tel}">
            <a class="esc" data-escolha="0" data-id="${tel}" data-atual="${per.dialogos[0].atual}" data-next="${per.dialogos[0].nx[0]}" onclick="enviar(this)">${per.dialogos[0].op[0]}</a>
            <a class="esc" data-escolha="1" data-id="${tel}" data-atual="${per.dialogos[0].atual}" data-next="${per.dialogos[0].nx[1]}" onclick="enviar(this)">${per.dialogos[0].op[1]}</a>
          </div>
          <div class="detalhes">
            <div class="header">
              <button class="btnbarranf" onclick="back(this)">
                <i class="fas fa-angle-left"></i>
                <div class="perfilcon" style="background-image: url(${per.imagem});"></div></button>
              <div class="ncon">${per.nome}</div>
            </div>
            <div class="container-detalhes">
              <div class="imagem-detalhes">
                <div style="background-image: url(${per.imagem});"></div>
              </div>
              <div class="nome-detalhes">${per.nome}</div>
              <div class="mais-detalhes">
                <div><span>Telefone</span><span>${per.id}</span></div>
                <div><span>Idade</span><span>${per.idade} anos</span></div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript" src="https://app.sollic.com/script-1.0.35.js"></script>
      </body>`);
    //localStorage.setItem("save", document.getElementById("body").innerHTML);
    window.location.href = newBlob(localStorage.getItem("id"+dataId.getAttribute("data-id")));
    
  }else{
    window.location.href = newBlob(`<!DOCTYPE html><html>${localStorage.getItem("id"+dataId.getAttribute("data-id"))}</html>`);
    //document.getElementById("w"+dataId.getAttribute("data-id")).style.visibility = "visible";
  }
}
const inputEle = document.getElementById('tel');
inputEle.addEventListener('keyup', function(e){
  var key = e.which || e.keyCode;
  if (key == 13) {
    addConversa();
  }
});
function addConversa(){
  var tel = document.getElementById("tel").value;
  var aviso = document.getElementById("aviso");

  if(personagem[tel]){
    if(!document.getElementById(personagem[tel].nome + tel)){
      var per = personagem[tel];
      var modal = document.getElementById("modal");
      var container = document.getElementById("container");

      modal.style.display = "none";
      container.innerHTML += `
      <div class="conversas" onclick="janelaConversa(this)" id="${per.nome}${tel}" data-id="${tel}">
        <div class="perfil" style="background-image: url(${per.imagem});"></div>
        <span class="nome">${per.nome}</span>
        <span class="mensagem">Nenhuma mensagem</span><span class="data"></span>
      </div>`;
      //localStorage.setItem("save", document.getElementById("body").innerHTML);
    }else{
      aviso.innerHTML = "Numero ja foi adicionado!";
      setTimeout(function(){
        aviso.innerHTML = "Digite um numero de telefone";
      },2000);
    }
  }else{
    aviso.innerHTML = "Numero invalido";
    setTimeout(function(){
      aviso.innerHTML = "Digite um numero de telefone";
    },2000);
  }
}
function novaConversa(){
  var modal = document.getElementById("modal");
  var fechar = document.getElementById("cmodal");
  modal.style.display = "block";
  document.getElementById("tel").focus();
  
  fechar.addEventListener("click", function(){
    modal.style.display = "none";
  });
}
function back(e){
  if(e.parentElement.parentElement.style.visibility == "visible"){
    e.parentElement.parentElement.removeAttribute("style");
  }
}
window.onload = function sumir(){
  if(!localStorage.getItem("sumiu")){
    document.getElementsByClassName("telafull")[0].style.display="block";
  }
};
if(salvar){
  var salvando = setInterval(function save(){
    if(!window.location.href.includes("blob")){
      localStorage.setItem(document.body.id, document.body.innerHTML);
    }
  }, 1);
  window.onload = salvando;
}
if(carregar){
  (function load(){
    if(document.body.id !== ""){
      if(localStorage.getItem(document.body.id) !== null && localStorage.getItem(document.body.id) !== ""){
        if(!window.location.href.includes("blob")){
          document.body.innerHTML = localStorage.getItem(document.body.id);
        }
      }
    }
  })();
}