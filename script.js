var upt = setInterval(function update(){
  var conversa = document.getElementsByClassName("conversas");

  for(var i = 0; i < conversa.length; i++){
    var mensagem = conversa[i].getElementsByClassName("mensagem")[0];
    if(mensagem){
      var ultima = document.querySelector("#d"+ conversa[i].getAttribute("data-id") +" > div:last-child > span");
      if(ultima){
        mensagem.innerHTML = ultima.innerHTML;
      }
    }
  }
},200);

var block = false;
var save = false;
function enviar(e){
  if(block === false){
    var idc = e.getAttribute("data-id");
    var esc = e.getAttribute("data-escolha");
    var next = e.getAttribute("data-next");
    var atual = e.getAttribute("data-atual");
    var dialogos = document.getElementById("d"+idc);
    var escbox = document.getElementById("esc"+idc);
    var msg = document.getElementsByClassName("msg");

    if(next != "fim"){
      dialogos.innerHTML += `<div><span class="msg dir">${e.innerHTML}</span></div>`;

      var segundos = personagem[idc].dialogos[atual].rs[esc].length;
      setTimeout(function(){
        if(personagem[idc].dialogos[atual].rs[esc] && personagem[idc].dialogos[atual].rs[esc] !== ""){
          dialogos.innerHTML += `<div><span class="msg esq">${personagem[idc].dialogos[atual].rs[esc]}</span></div>`;
        }
        escbox.innerHTML = `
        <div class="escbox" id="esc${idc}">
          <a class="esc" data-escolha="0" data-id="${idc}" data-atual="${next}" data-next="${personagem[idc].dialogos[next].nx[0]}" onclick="enviar(this)">${personagem[idc].dialogos[next].op[0]}</a>
          <a class="esc" data-escolha="1" data-id="${idc}" data-atual="${next}" data-next="${personagem[idc].dialogos[next].nx[1]}" onclick="enviar(this)">${personagem[idc].dialogos[next].op[1]}</a>
        </div>`;
        block = false;
        msg[(msg.length - 1)].scrollIntoView({behavior: 'smooth' });
      },segundos*60);
      msg[(msg.length - 1)].scrollIntoView({behavior: 'smooth' });
    }
  }
  block = true;
}
function janelaConversa(dataId){
  if(!document.getElementById(dataId.getAttribute("data-id"))){
    var tel = dataId.getAttribute("data-id");
    var per = personagem[tel];
    document.body.innerHTML += `
    <div id="${tel}" class="wincon">
      <div class="header">
        <button class="btnbarranf" onclick="back(this)"><i class="fas fa-angle-left"></i> Voltar</button>
        <div class="ncon">${per.nome}</div>
      </div>
      <div class="dialogos" id="d${tel}"></div>
      <div class="escbox" id="esc${tel}">
        <a class="esc" data-escolha="0" data-id="${tel}" data-atual="${per.dialogos[0].atual}" data-next="${per.dialogos[0].nx[0]}" onclick="enviar(this)">${per.dialogos[0].op[0]}</a>
        <a class="esc" data-escolha="1" data-id="${tel}" data-atual="${per.dialogos[0].atual}" data-next="${per.dialogos[0].nx[1]}" onclick="enviar(this)">${per.dialogos[0].op[1]}</a></div>
    </div>`;
  }else{
    document.getElementById(dataId.getAttribute("data-id")).style.display = "block";
  }
}
function addConversa(){
  var tel = document.getElementById("tel").value;
  var aviso = document.getElementById("aviso");

  if(personagem[tel]){
    if(!document.getElementById(personagem[tel].nome + tel)){
      var per = personagem[tel];
      var modal = document.getElementById("modal");
      var container = document.getElementById("container");

      modal.style.display = "none";
      container.innerHTML += `
      <div class="conversas" onclick="janelaConversa(this)" id="${per.nome}${tel}" data-id="${tel}">
        <div class="perfil" style="background-image: url(${per.imagem});"></div>
        <span class="nome">${per.nome}</span>
        <span class="mensagem">Nenhuma mensagem</span><span class="data"></span>
      </div>`;
    }else{
      aviso.innerHTML = "Numero ja foi adicionado!";
      setTimeout(function(){
        aviso.innerHTML = "Digite um numero de telefone";
      },2000);
    }
  }else{
    aviso.innerHTML = "Numero invalido";
    setTimeout(function(){
      aviso.innerHTML = "Digite um numero de telefone";
    },2000);
  }
}
function novaConversa(){
  var modal = document.getElementById("modal");
  var fechar = document.getElementById("cmodal");
  modal.style.display = "block";
  document.getElementById("tel").focus();
  
  fechar.addEventListener("click", function(){
    modal.style.display = "none";
  });
}
function back(e){
  e.parentElement.parentElement.style.display="none";
}
window.onload = function sumir(){
  if(!localStorage.getItem("sumiu")){
    document.getElementsByClassName("telafull")[0].style.display="block";
  }
};
if(save){
  document.getElementById("body").addEventListener("click", function save(){
    localStorage.setItem("save", document.getElementById("body").innerHTML);
  });
  (function load(){
    if(localStorage.getItem("save")){
      document.getElementById("body").innerHTML = localStorage.getItem("save");
    }
  })();
}